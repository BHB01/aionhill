<?php
/**
 * Created by PhpStorm.
 * User: balintharangozo
 * Date: 2020. 03. 22.
 * Time: 19:12
 */

namespace AionHill\Dump\Console\Command;

use Magento\Reports\Model\ResourceModel\Product\CollectionFactory;
use phpDocumentor\Reflection\Types\Collection;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Class ProductDump
 *
 * Contains the main logic of the dump module. This module dumps the simple products of the system with the following
 * attributes:
 * - ID
 * - SKU
 * - Type
 * - Stock Status
 * - Name
 * - Quantity
 *
 */
class ProductDump extends Command
{
    /**
     * @var \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory
     */
    protected $productCollectionFactory;
    /**
     * @var \Magento\Catalog\Model\Product\Visibility
     */
    protected $productVisibility;
    /**
     * @var \Magento\CatalogInventory\Model\Stock\StockItemRepository
     */
    protected $stockItemRepository;

    /**
     * ProductDump constructor.
     * @param \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory
     * @param \Magento\Catalog\Model\Product\Visibility $productVisibility
     * @param \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository
     */
    public function __construct(
        \Magento\Catalog\Model\ResourceModel\Product\CollectionFactory $productCollectionFactory,
        \Magento\Catalog\Model\Product\Visibility $productVisibility,
        \Magento\CatalogInventory\Model\Stock\StockItemRepository $stockItemRepository
    ) {
        $this->productCollectionFactory = $productCollectionFactory;
        $this->productVisibility        = $productVisibility;
        $this->stockItemRepository      = $stockItemRepository;
        parent::__construct();
    }

    /**
     * Required method of magento CLI Commands.
     */
    protected function configure()
    {
        $this->setName('dump:products');
        $this->setDescription('This module dumps the simple products of the system with predefined attributes.');
        parent::configure();
    }

    /**
     * Evaluation part of the command, contains the main logic
     *
     * @param InputInterface $input
     * @param OutputInterface $output
     *
     * @return null|int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /* Question for the query with user input */
        $helper         = $this->getHelper('question');

        $productTypes   = [
            'Simple products'   => \Magento\Catalog\Model\Product\Type::TYPE_SIMPLE,
            'Virtual products'  => \Magento\Catalog\Model\Product\Type::TYPE_VIRTUAL,
            'Bundle products'   => \Magento\Catalog\Model\Product\Type::TYPE_BUNDLE
        ];
        $typeQuestions  = [
            '1' => 'Simple products',
            '2' => 'Virtual products',
            '3' => 'Bundle products',
            '4' => 'All products'
        ];
        $question = new ChoiceQuestion(
            'What type of product do you want to dump? (default: All products)',
            $typeQuestions,
            4
        );

        $question->setErrorMessage('Your choice "%s" is invalid. Please choose a number from the list above.');

        $selectedProductType = $helper->ask($input, $output, $question);
        $output->writeln('You have just selected: '.$selectedProductType);

        /** @var array $productTableRows - contains the rows of the rendered product table */
        $productTableRows = [];

        /** @var \Magento\Catalog\Model\ResourceModel\Product\Collection $collection */
        $collection = $this->productCollectionFactory->create();
        $collection->addAttributeToSelect('*');
        if (isset($productTypes[$selectedProductType])) {
            $collection->addAttributeToFilter('type_id', $productTypes[$selectedProductType]);
        }

        if ($collection->getSize()) {
            foreach ($collection as $product) {
                $stockInformations = $this->stockItemRepository->get($product->getId());
                $productTableRows[] = [
                    $product->getId(),
                    $product->getSku(),
                    $product->getTypeId(),
                    ($stockInformations->getIsInStock() === true ? 'In stock' : 'Out of stock'),
                    $product->getName(),
                    $stockInformations->getQty()
                ];
            }
            /** @var Table $table */
            $table = new Table($output);
            $table
                ->setHeaders(['ID', 'SKU','Type', 'Status', 'Name', 'Qty'])
                ->setStyle('box-double')
                ->setRows($productTableRows);
            $table->render();
        } else {
            $output->writeln('<error>There are no items with the given conditions.</error>');
        }

        $question = new ConfirmationQuestion(
            'Would you like another dump? (y/N)',
            false,
            '/^(y|i)/i'
        );
        if ($helper->ask($input, $output, $question)) {
            return $this->execute($input, $output);
        }
    }
}
